import {setCookie,getCookie} from "../cookie/cookie.js";

layui.use(['form', 'layer'],function() {
    var form = layui.form,
        layer = layui.layer;
    form.verify({
        btnPath: function(value){
            if (value.length === 0){
                return "不能为空";
            }
            if(/[\u4E00-\u9FA5]/g.test(value)){
                return '不准输入中文';
            }
        }
    });

    let state = false;

    //监听提交
    form.on('submit(add)',
        function(data) {
            if (state){
                var obj = data.field;
                obj.id = "btn"+new Date().getTime()+"and"+window.parent.$('#principalHost').text();
                obj.btnPath = obj.btnPath.replaceAll('/','$');
                let str = JSON.stringify(obj);
                getCookie("jsonAndCookie") == "true"?
                $.ajax({
                    url:'/addBtnObj',
                    method:'post',
                    data:str,
                    dataType:'JSON',
                    contentType: "application/json; charset=utf-8",
                    success:function (res) {
                        ok();
                    }
                }):setCookie(obj.id,str,"d30");
                ok();
            }else {
                layer.msg("核对路径再进行添加", {icon: 5});
            }
            return false;
        });

    $('#btnPath').blur(function (){
        $.get('/checkPath',{path:$(this).val()},function (res) {
            if (res.code == 200){
                state = true;
            }else {
                state = false;
                layer.msg(res.msg, {icon: 5});
            }
        })
    });

});

function ok(){
layer.alert("增加成功", {
        icon: 6
    },
    function() {
        //关闭当前frame
        xadmin.close();

        // 可以对父窗口进行刷新
        xadmin.father_reload();
    });
}