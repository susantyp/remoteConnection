layui.use('layer', function () {
    var layer = layui.layer;
    $('#deploy').click(function () {
       window.open($('#welcomeRequestUrl').val()+"/downExe")
    });
    $('#unfoldLogBig').click(function (){
        $(this).hide();
        $('#logDiv').show();
    });
    $('#unfoldLogEnd').click(function (){
        $('#logDiv').hide();
        $('#unfoldLogBig').show();
    });
    rendererText();
});

function rendererText(){
    let date = new Date();
    let month = date.getMonth() + 1;
    switch (month){
        case 1:
            text("临近新年，放下步伐，人生为棋，我愿为卒，行动虽慢，可谁又曾看见我后退一步");
            break;
        case 2:
            text("新春快乐,虎年大吉,顺风顺水,前程似锦,以梦为马不负韶华。");
            break;
        case 3:
        case 4:
        case 5:
            text("人有了信念和追求就能忍受一切艰苦");
            break;
        case 6:
            text("要成功，先发疯，下定决心往前冲,高考加油！");
            break;
        case 7:
        case 8:
        case 9:
            text("只会在水泥地上走路的人，永远不会留下深深的脚印");
            break;
        case 10:
            text("国庆节快乐");
            break;
        case 11:
        case 12:
            text("一年将近到头，想想你都做了什么？");
            break;
        default:
            text("欢迎预览");
    }
}

function text(text){
    $('#greeting').text(text);
}