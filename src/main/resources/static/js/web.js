import {setCookie,getCookie} from "./cookie/cookie.js";

$(function (){

    /**
     * 关闭浏览器右键
     */
    $(document).bind("contextmenu",function(e){
        return false;
    });

    /**
     * 弹出设置
     */
    let cookieName = getCookie("cookieName");
    if(cookieName === null || cookieName === ""){
        setCookie("cookieName","0","d30");
    }
    if (getCookie("cookieName") === "0"){
        layui.use("layer", function () {
            var layer = layui.layer;
            layer.open({
                type: 1
                ,title: false //不显示标题栏
                ,closeBtn: false
                ,area: '300px;'
                ,shade: 0.8
                ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                ,btn: ['不再提示', '我知道了']
                ,btnAlign: 'c'
                ,moveType: 1 //拖拽模式，0或者1
                ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">hello 帅小伙!<br> 项目还存在很多缺陷，代码中还存在未修复的bug，如果感兴趣的小伙伴可以深度进去探讨研究</div>'
                ,success: function(layero){
                    $('.layui-layer-btn0').click(function (){
                        setCookie("cookieName","1","d30");
                    });
                }
            });
        });
    }

});