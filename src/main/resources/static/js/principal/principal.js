import {getBtnCookieList, getCookie, getCookieList} from "../cookie/cookie.js";

layui.use(['form', 'layer'],function() {
    var  layer = layui.layer;
    let btnCookieList = getBtnCookieList($('#principalHost').text());
    getCookie("jsonAndCookie") == "true"?
        $.get('/btnList/'+$('#principalHost').text(),{},function (res) {
            dataRender(res.data);
    }):dataRender(btnCookieList);
});

function dataRender(data){
    if (data != null){
        $.each(data,function (index,item){
            if (item.showId == 0){
                let btn = '<button type="button" id="'+item.id+'" title="'+item.btnPath.replaceAll('$','/')+'" onclick="route(\''+item.btnPath.replaceAll('$','/')+'\',2)" class="layui-btn layui-btn-sm addBtnCookie">跳转 '+item.btnPath.replaceAll('$','/')+'</button>';
                $('#addBtnList').append(btn);
            }
        });
    }
}