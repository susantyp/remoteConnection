import {getBtnCookieList, delCookie, getCookie, getCookieList} from "../cookie/cookie.js";
layui.use(['table','layer'], function() {

    var table = layui.table,layer = layui.layer,$ = layui.jquery;
    let host = window.parent.$('#principalHost').text();
    jsonList(table,host);

    table.on('tool(addBtnList)', function(obj) {
        var d = obj.data;
        if (obj.event === 'del'){
            layer.confirm('确认要'+d.id+'删除吗？', function (index) {
                getCookie("jsonAndCookie") == "true"?
                $.get('/delBtnObj/'+host+"/"+d.id,{},function (res) {
                    xadmin.father_reload();
                }):delCookie(d.id);
                layer.msg('已删除!', {icon: 1, time: 1000});
                xadmin.father_reload();
            });
        }
        if (obj.event === 'cd'){
            xadmin.close();
            window.parent.route(d.btnPath.replaceAll('$','/'),2);
        }
    });

});

function jsonList(table,host){
    getCookie("jsonAndCookie") == "true"?
    $.get('/btnList/'+host,{},function (res) {
        tableReadr(table,res.data);
    }):tableReadr(table,getBtnCookieList(host));
}

function tableReadr(table,data){
    table.render({
        elem:'#addBtnList',
        cols:[
            [
                {field:'id',title:'ID',sort:true,width:260},
                {field: 'btnPath',title:'按钮路径',templet:function (d) {
                        return '<span>'+d.btnPath.replaceAll('$','/')+'</span>'
                    }},
                {field: 'showId',title: '显示位置',templet:function (d) {
                        return '<span>'+(d.showId == 0?"首页":"按钮层")+'</span>'
                    }},
                {title: '操作',width:150,templet:function (d){
                        return '<button type="button" lay-event="cd" class="layui-btn  layui-btn-xs layui-btn-normal">进入</button>'+
                            '<button type="button" lay-event="del" class="layui-btn layui-btn-xs  layui-btn-danger">删除</button>'
                    }}
            ]
        ],
        data:data==null?[]:data,
        page: true
    });
}