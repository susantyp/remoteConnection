import {setCookie,getNowTime,getCookie} from "../cookie/cookie.js";

layui.use(['form', 'layer'],function() {
    var form = layui.form,
        layer = layui.layer;

    //监听提交
    form.on('submit(add)',
        function(data) {
            var obj = data.field;
            obj.id = "connectData"+new Date().getTime();
            obj.lastTime = getNowTime();
            if (obj.survival === "" || obj.survival === null){
                obj.survival = "30";
                obj.survivalUnit = "d";
            }
            obj.timedOut = obj.timedOut === null || obj.timedOut === ""?"5000":obj.timedOut;
            obj.port = obj.port === null || obj.port === ""?"21":obj.port;
            obj.survivalTime = obj.survivalUnit+obj.survival;
            let str = JSON.stringify(obj);
            getCookie("jsonAndCookie") == "true"?
                $.ajax({
                    url:'/addLoginObj',
                    method:'post',
                    data:str,
                    dataType:'JSON',
                    contentType: "application/json; charset=utf-8",
                    success:function (res) {
                    }
                })
                :setCookie(obj.id,str,obj.survivalTime);

            layer.alert("增加成功", {
                    icon: 6
                },
                function() {
                    //关闭当前frame
                    xadmin.close();
                    // 可以对父窗口进行刷新
                    xadmin.father_reload();
                });
            return false;
        });
});