package com.ftp.longdistance.service.service;

import com.ftp.longdistance.bean.vo.FileVo;
import com.ftp.longdistance.bean.vo.PathVo;
import com.ftp.longdistance.ftpmapper.FtpFile;
import com.ftp.longdistance.ftpmapper.SftpFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>
 * </p>
 */
public interface FileFtpService{


    /**
     * 返回当前路径下面的文件
     * @param path
     * @param request
     * @return
     */
    List<FileVo> list(String path, HttpServletRequest request);

    /**
     * 返回前端显示的数据
     * @param path
     * @param request
     * @return
     */
    Map<String,Object> allList(@RequestBody PathVo path, HttpServletRequest request);

    /**
     * 获取文件权限
     * @param path
     * @param request
     * @return
     */
    String fileAuthority(String path,HttpServletRequest request);

    /**
     * 获取文件大小
     * @param path
     * @param request
     * @return
     */
    String fileSize(String path,HttpServletRequest request);

    /**
     * 文件下载
     * @param path
     * @param fileName
     */
    void download(String path, String fileName);


    /**
     * 文件删除方法
     * @param filePath
     * @param fileName
     */
    void fileDelete(String filePath,String fileName);


    /**
     * 更改文件名称
     * @param oldPath
     * @param newPath
     */
    void rename(String oldPath,String newPath);


    /**
     * 文件上传
     * @param file
     * @param request
     */
    void upload( MultipartFile file,HttpServletRequest request);


    /**
     * 处理集合
     * @param pathObj
     * @param path
     * @param request
     * @param currentDirectory
     * @return
     */
    List<FileVo> pathList(PathVo pathObj,String path,HttpServletRequest request,String currentDirectory);


    /**
     * 转换FileVo对象
     * @param path
     * @param list
     * @return
     */
    List<FileVo> transitionFileVo(String path, List<String> list);

    /**
     * 处理路径
     * @param path
     * @param request
     * @return
     */
    List<FileVo> judgeThePath(PathVo path, HttpServletRequest request);



    /**
     * 预览文件
     * @param response
     * @param serverPath
     * @param fileName
     */
    void fileView(HttpServletResponse response,String serverPath,String fileName);
}
