package com.ftp.longdistance.service.service;

import com.ftp.longdistance.bean.vo.BtnVo;
import com.ftp.longdistance.bean.vo.FtpVo;

import java.util.List;

/**
 * @author TanYongPeng
 * <p>
 * </p>
 */
public interface SaveJsonLocalhostService {

    /**
     * 添加登录数据
     * @param ftpVo
     */
    void addLoginObj(FtpVo ftpVo);


    /**
     * 获取所有登录数据
     * @return
     */
    List<FtpVo> loginList();


    /**
     * 删除登录数据
     * @param id
     */
    void delLoginObj(String id);


    /**
     * 添加按钮的数据
     * @param btnVo
     */
    void addBtnObj(BtnVo btnVo);


    /**
     * 获取所有按钮数据
     * @param host
     * @return
     */
    List<BtnVo> btnList(String host);

    /**
     * 删除按钮
     * @param host
     * @param btnId
     */
    void delBtnObj(String host,String btnId);
}
