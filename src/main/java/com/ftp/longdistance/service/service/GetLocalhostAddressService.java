package com.ftp.longdistance.service.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 * </p>
 */
public interface GetLocalhostAddressService {

    /**
     * 获取当前服务器地址
     * @param request
     * @return
     */
    String getLocalhost(HttpServletRequest request);

    /**
     * 获取服务器地址和端口号
     * @param request
     * @return
     */
    String getHostPort(HttpServletRequest request);

}
