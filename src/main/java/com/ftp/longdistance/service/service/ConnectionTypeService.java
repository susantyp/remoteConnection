package com.ftp.longdistance.service.service;



import java.util.List;

/**
 * @author tanyongpeng
 * <p>
 * </p>
 */
public interface ConnectionTypeService {

    /**
     * 查询所有连接方式
     * @return
     */
    List<String> connectionTypeList();

}
