package com.ftp.longdistance.service.service;

import com.ftp.longdistance.bean.Ftp;
import com.ftp.longdistance.ftpmapper.service.PointOfJunction;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 * </p>
 */
public interface LoginService {

    /**
     * 连接登录
     * @param ftp
     * @param request
     * @param p
     * @return
     */
    boolean login(@RequestBody Ftp ftp, HttpServletRequest request, PointOfJunction p);

    /**
     * 退出登录
     * @param request
     * @param p
     */
    void loginOut(HttpServletRequest request,PointOfJunction p);

}
