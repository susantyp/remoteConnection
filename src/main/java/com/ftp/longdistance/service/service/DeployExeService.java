package com.ftp.longdistance.service.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author TanYongPeng
 * <p>
 * </p>
 */
public interface DeployExeService {

    /**
     * 下载本地exe文件
     * @param path
     * @param fileName
     * @param request
     * @param response
     */
    void downExe(String path,String fileName, HttpServletRequest request, HttpServletResponse response);

}
