package com.ftp.longdistance.service.service;


import com.ftp.longdistance.bean.vo.ParameterCommVo;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author TanYongPeng
 * <p>
 *   默认空实现
 *   接口适配器
 * </p>
 */
public abstract class AbsAdapterCommand implements SelectCommandService {

    @Override
    public Map<String, Object> rmrf(ParameterCommVo parameterCommVo) {
        return parameterCommVo.getMaps();
    }

    @Override
    public Map<String, Object> mkdir(ParameterCommVo parameterCommVo) {
        return parameterCommVo.getMaps();
    }

    @Override
    public Map<String, Object> cd(ParameterCommVo parameterCommVo) {
        return parameterCommVo.getMaps();
    }
}
