package com.ftp.longdistance.service.service;


import com.ftp.longdistance.annotation.CommandName;
import com.ftp.longdistance.bean.vo.ParameterCommVo;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>
 *     (建造者)具体抽象
 *     (适配器)接口适配器
 *     命令接口
 */
public interface SelectCommandService {

    /**
     * 删除文件夹或文件
     * @param parameterCommVo
     * @return
     */
    @CommandName(comm = "rmrf",des = "删除")
    Map<String,Object> rmrf(ParameterCommVo parameterCommVo);


    /**
     * 创建文件夹
     * @param parameterCommVo
     * @return
     */
    @CommandName(comm = "mkdir",des = "创建")
    Map<String,Object> mkdir(ParameterCommVo parameterCommVo);


    /**
     * 进入当前目录
     * @param parameterCommVo
     * @return
     */
    @CommandName(comm = "cd",des = "跳转")
    Map<String,Object> cd(ParameterCommVo parameterCommVo);


}
