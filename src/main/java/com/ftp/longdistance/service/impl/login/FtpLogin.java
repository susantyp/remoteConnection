package com.ftp.longdistance.service.impl.login;

import com.ftp.longdistance.annotation.FileType;
import com.ftp.longdistance.bean.Ftp;
import com.ftp.longdistance.ftpmapper.service.PointOfJunction;


import com.ftp.longdistance.service.service.LoginService;
import com.ftp.longdistance.util.LoginUtils;
import com.ftp.longdistance.util.PathUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 *     ftP 登录方式
 * </p>
 */
@Service("ftpLogin")
@FileType("FTP")
public class FtpLogin implements LoginService {

    @Override
    public boolean login(Ftp ftp, HttpServletRequest request,PointOfJunction p) {
        return LoginUtils.login(ftp,request,p);
    }

    @Override
    public void loginOut(HttpServletRequest request,PointOfJunction p) {
        LoginUtils.loginOut(request,p);
    }


}
