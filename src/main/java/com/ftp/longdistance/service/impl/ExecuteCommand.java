package com.ftp.longdistance.service.impl;


import com.ftp.longdistance.annotation.FileType;
import com.ftp.longdistance.bean.vo.ParameterCommVo;
import com.ftp.longdistance.factory.InstantiationFactory;
import com.ftp.longdistance.ftpmapper.service.PointOfJunction;
import com.ftp.longdistance.service.impl.connect.FileFtpServiceImpl;
import com.ftp.longdistance.service.service.AbsAdapterCommand;
import com.ftp.longdistance.service.service.SelectCommandService;
import com.ftp.longdistance.util.PathUtils;
import com.ftp.longdistance.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>
 *     指挥者
 *     执行命令
 * </p>
 */
@Component
public class ExecuteCommand {


    @Autowired
    InstantiationFactory instantiationFactory;

    /**
     * 执行命令
     * @param order
     * @param request
     * @return
     */
    public Map<String, Object> executeCommand(String order, HttpServletRequest request){
        SelectCommandService command = instantiationFactory.command(request);
        HashMap<String,Object> map = new HashMap<>();
        ParameterCommVo parameterCommVo = new ParameterCommVo(order,request,map);
        String commName = StringUtils.disposeChar(order)[0];
        if ("cd".equals(commName)){
            command.cd(parameterCommVo);
        }else if("mkdir".equals(commName)){
            command.mkdir(parameterCommVo);
        }else if("rmrf".equals(commName)){
            command.rmrf(parameterCommVo);
        }
        return parameterCommVo.getMaps();
    }


}
