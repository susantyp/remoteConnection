package com.ftp.longdistance.service.impl;

import com.ftp.longdistance.service.service.GetLocalhostAddressService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 *     返回本地地址
 * </p>
 */
@Service
public class GetLocalhostAddressServiceImpl implements GetLocalhostAddressService {

    @Override
    public String getLocalhost(HttpServletRequest request) {
        String port = request.getLocalPort() == 80?"":":"+request.getLocalPort();
        return request.getScheme()+"://"+request.getServerName()+port;
    }

    @Override
    public String getHostPort(HttpServletRequest request) {
        return request.getServerName()+":"+request.getLocalPort();
    }
}
