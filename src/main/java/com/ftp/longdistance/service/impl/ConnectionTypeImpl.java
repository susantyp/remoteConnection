package com.ftp.longdistance.service.impl;


import com.ftp.longdistance.service.service.ConnectionTypeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tanyongpeng
 * <p>
 *     添加连接方式  xml文件可配置
 * </p>
 */
@Service
public class ConnectionTypeImpl implements ConnectionTypeService {

    @Value("${connection.type}")
    private List<String> type;

    @Override
    public List<String> connectionTypeList() {
        return type;
    }


}
