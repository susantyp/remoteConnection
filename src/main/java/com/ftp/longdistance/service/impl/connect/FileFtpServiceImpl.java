package com.ftp.longdistance.service.impl.connect;


import com.ftp.longdistance.bean.vo.ParameterCommVo;
import com.ftp.longdistance.util.TransitionUtils;
import com.ftp.longdistance.annotation.FileType;
import com.ftp.longdistance.ftpmapper.FtpFile;

import com.ftp.longdistance.bean.vo.FileVo;
import com.ftp.longdistance.bean.vo.PathVo;

import com.ftp.longdistance.exception.BizException;

import com.ftp.longdistance.service.service.AbsAdapterCommand;
import com.ftp.longdistance.util.PathUtils;

import com.ftp.longdistance.service.service.FileFtpService;
import com.ftp.longdistance.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author tanyongpeng
 * <p>
 *     ftp实现
 * </p>
 */
@Service
@Slf4j
@FileType("FTP")
public class FileFtpServiceImpl extends AbsAdapterCommand implements FileFtpService  {


    @Override
    public List<FileVo> list(String path ,HttpServletRequest request) {

        try {
            List<String> strings = FtpFile.INSTANCE.listFiles(path);
            return transitionFileVo(path,strings);
        }catch (Exception e){
            throw new BizException("101","文件路径异常");
        }
    }

    @Override
    public List<FileVo> judgeThePath(PathVo path, HttpServletRequest request){
        List<FileVo> list = null;
        if (path.getCloseOrRefund() == 0){
            String currentDirectory = PathUtils.currPath(request);
            String objPath = path.getPath()==null?"":path.getPath();
            String paths = currentDirectory+objPath;
            paths = paths.replace("//","/");
            path.setPath(paths);
            list = this.list(PathUtils.checkPath(paths), request);
        }else if (path.getCloseOrRefund() == 1){
            String currentDirectory = PathUtils.currPath(request);
            if (!"/".equals(currentDirectory)) {
                list = pathList(path, PathUtils.checkPath(PathUtils.newPath(currentDirectory)),request, PathUtils.newPath(currentDirectory));
            }else {
                list = pathList(path,"/",request,"/");
            }
        }else if (path.getCloseOrRefund() == 2){
            list = pathList(path,PathUtils.checkPath(path.getPath()),request,PathUtils.lastCheckPath(path.getPath()));
        }
        return list;
    }

    @Override
    public Map<String, Object> allList(PathVo path, HttpServletRequest request) {
        return PathUtils.mapAddPath(judgeThePath(path, request),path,request);
    }

    @Override
    public List<FileVo> pathList(PathVo pathObj,String path,HttpServletRequest request,String currentDirectory){
        pathObj.setPath(currentDirectory);
        return list(path,request);
    }

    @Override
    public List<FileVo> transitionFileVo(String path, List<String> list) {
        return TransitionUtils.INSTANCE.transitionFileVo(path,list,FtpFile.INSTANCE);
    }

    @Override
    public String fileAuthority(String path, HttpServletRequest request) {
        return FtpFile.INSTANCE.fileAuthority(path);
    }

    @Override
    public String fileSize(String path, HttpServletRequest request) {
        return FtpFile.INSTANCE.fileSize(path);
    }

    @Override
    public void download(String path, String fileName) {
        FtpFile.INSTANCE.download(path, fileName);
    }


    /**
     * 进入目录
     * @param par
     * @return
     */
    @Override
    public Map<String,Object> cd(ParameterCommVo par) {
        String currPath = PathUtils.currPath(par.getRequest());
        String order = StringUtils.disposeChar(par.getParameter())[1];
        log.info("命令cd  参数 = {}",order);
        if (FtpFile.INSTANCE.checkExist(order)) {
            //存在，不用拼接
            order = PathUtils.firstCheckPath(order);
            List<FileVo> list = this.list(PathUtils.checkPath(order), par.getRequest());
            par.getRequest().getSession().setAttribute("currentDirectory", PathUtils.lastCheckPath(order));
            par.getMaps().put("currentDirectory", PathUtils.lastCheckPath(order));
            par.getMaps().put("listPath",list);
        }else {
            // 不存在 开始拼接
            String path = currPath+order;
            List<FileVo> list = this.list(PathUtils.checkPath(path), par.getRequest());
            par.getRequest().getSession().setAttribute("currentDirectory", PathUtils.lastCheckPath(path));
            par.getMaps().put("currentDirectory", PathUtils.lastCheckPath(path));
            par.getMaps().put("listPath",list);
        }
        return par.getMaps();
    }


    /**
     * 创建文件夹
     * @param par
     * @return
     */
    @Override
    public Map<String,Object> mkdir(ParameterCommVo par){
        FtpFile.INSTANCE.dir(PathUtils.checkPath(PathUtils.currPath(par.getRequest()))+ StringUtils.disposeChar(par.getParameter())[1]);
        return PathUtils.mapAddPath(par.getMaps(),par.getRequest(),list(PathUtils.checkPath(PathUtils.currPath(par.getRequest())), par.getRequest()));
    }

    /**
     * 删除文件夹或者文件
     * @param par
     * @return
     */
    @Override
    public Map<String,Object> rmrf(ParameterCommVo par){
        String path =  PathUtils.checkPath(PathUtils.currPath(par.getRequest()));
        String fileName = StringUtils.disposeChar(par.getParameter())[1];
        if (FtpFile.INSTANCE.checkFileName(path+fileName)) {
            FtpFile.INSTANCE.deleteDir(path+fileName);
        }else {
            FtpFile.INSTANCE.delete(path,fileName);
        }
        return PathUtils.mapAddPath(par.getMaps(),par.getRequest(),list(PathUtils.checkPath(PathUtils.currPath(par.getRequest())), par.getRequest()));
    }


    @Override
    public void fileView(HttpServletResponse response,String serverPath,String fileName) {
        FtpFile.INSTANCE.fileView(response,serverPath,fileName);
    }


    @Override
    public void fileDelete(String filePath,String fileName) {
        FtpFile.INSTANCE.delete(filePath,fileName);
    }

    @Override
    public void rename(String oldPath, String newPath) {
        FtpFile.INSTANCE.rename(oldPath,newPath);
    }

    @Override
    public void upload(MultipartFile file,HttpServletRequest request) {
        FtpFile.INSTANCE.upload(PathUtils.currPath(request),file);
    }


}
