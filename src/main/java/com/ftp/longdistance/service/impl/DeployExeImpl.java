package com.ftp.longdistance.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import com.ftp.longdistance.service.service.DeployExeService;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @author TanYongPeng
 * <p>
 *     下载  exe 实现
 * </p>
 */
@Service
public class DeployExeImpl implements DeployExeService {

    @Override
    public void downExe(String path,String fileName, HttpServletRequest request, HttpServletResponse response) {
        if ("Linux".equals(System.getProperty("os.name"))){
            try {
                File file = new File(path+fileName);
                response.reset();
                response.setCharacterEncoding("UTF-8");
                response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("utf-8"), "iso8859-1"));
                response.setHeader("Content-Length", String.valueOf(file.length()));
                ServletOutputStream outputStream = response.getOutputStream();
                byte[] bytes = FileUtil.readBytes(path+fileName);
                outputStream.write(bytes);
                IoUtil.close(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
