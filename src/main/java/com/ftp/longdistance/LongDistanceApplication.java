package com.ftp.longdistance;


import com.ftp.longdistance.system.SystemSettings;
import com.ftp.longdistance.thread.ExeThread;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author tanyongpeng
 */

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class LongDistanceApplication {

    public static void main(String[] args) {
       ExeThread.INSTANCE.run(LongDistanceApplication.class,args);
    }

}
