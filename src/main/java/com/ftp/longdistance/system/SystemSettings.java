package com.ftp.longdistance.system;

import com.ftp.longdistance.config.CommandLineRunnerImpl;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.text.NumberFormat;


/**
 * @author tanyongpeng
 * <p>
 *     系统配置
 * </p>
 */
public class SystemSettings{

    private SystemSettings() {
    }

    public static volatile SystemSettings sys;

    public static SystemSettings getInstance(){
        if (sys == null){
            synchronized (SystemSettings.class){
                if (sys == null){
                    sys =  new SystemSettings();
                }
            }
        }
        return sys;
    }

    public SystemSettings comm(Class<?> primarySource,String... args){
        this.setPrimarySource(primarySource);
        this.setArgs(args);
        return SystemSettings.getInstance();
    }

    public SystemSettings comm(boolean log,Class<?> primarySource,String... args){
        this.setOpenTheLog(log);
        this.setPrimarySource(primarySource);
        this.setArgs(args);
        return SystemSettings.getInstance();
    }

    @Getter
    private boolean openTheLog = true;
    @Getter
    private Class<?> primarySource;
    @Getter
    private String[] args;

    private void setPrimarySource(Class<?> primarySource) {
        this.primarySource = primarySource;
    }

    private void setArgs(String[] args) {
        this.args = args;
    }

    private void setOpenTheLog(boolean openTheLog) {
        this.openTheLog = openTheLog;
    }

    public SystemSettings run(){
        new CommandLineRunnerImpl().run(getPrimarySource(),getArgs());
        return SystemSettings.getInstance();
    }



}
