package com.ftp.longdistance.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>
 *     路径 cd  ../
 * </p>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PathVo {

    /**
     * 当前路径
     */
    private String path;

    /**
     * 当前是进目录，还是退目录
     * 0 == 进   1 == 退
     */
    private Integer closeOrRefund;
}
