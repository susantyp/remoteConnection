package com.ftp.longdistance.bean.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author tanyongpeng
 * <p>
 *     前端按钮
 * </p>
 */
@ToString
public class BtnVo {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private Integer showId;

    @Setter
    private String btnPath;


    public String getBtnPath() {
        return btnPath.replaceAll("\\$","/");
    }

}
