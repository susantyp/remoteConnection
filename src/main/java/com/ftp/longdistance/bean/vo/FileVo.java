package com.ftp.longdistance.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>
 *     前端文件展示
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileVo {

    private String fileName;

    private Integer isDir;

}
