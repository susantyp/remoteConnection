package com.ftp.longdistance.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>
 *     命令参数
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParameterCommVo {

    private String parameter;

    private HttpServletRequest request;

    private Map<String,Object> maps;

}
