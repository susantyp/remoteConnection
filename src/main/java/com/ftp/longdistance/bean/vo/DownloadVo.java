package com.ftp.longdistance.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>
 *     下载文件
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DownloadVo {

    private String path;

    private String fileName;

}
