package com.ftp.longdistance.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TanYongPeng
 * <p>
 *     命令详细，前端显示
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommandVo {

    private int id;

    private String comm;

    private String commType;

    private String createTime;

    private String des;

}
