package com.ftp.longdistance.bean.vo;

import lombok.Getter;
import org.springframework.stereotype.Component;

/**
 * @author TanYongPeng
 * <p>
 *     下载exe文件
 * </p>
 */
@Getter
@Component
public class ExeVo {

    public String path = "/home/download/";

    public String fileName = "远程连接.exe";

}
