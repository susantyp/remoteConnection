package com.ftp.longdistance.bean.vo;

import com.ftp.longdistance.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author TanYongPeng
 * <p>
 *     获取命令详细信息，用于前端展示
 * </p>
 */
@Component
@ConfigurationProperties("custom")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommandListAllVo {
    private List<CommandVo> list;

    public List<CommandVo> getList() {
        return list;
    }
}
