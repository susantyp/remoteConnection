package com.ftp.longdistance.bean.vo;

import com.ftp.longdistance.bean.Ftp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TanYongPeng
 * <p>
 *     转json
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FtpVo extends Ftp {

    private String id;

    private String survival;

    private String survivalTime;

    private String survivalUnit;

    private String lastTime;

}
