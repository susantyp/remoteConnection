package com.ftp.longdistance.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author TanYongPeng
 * <p>
 *     连接服务器
 * </p>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ftp {

    /**
     * //ip地址
     */
    private String ftpHost;

    /**
     * //端口号
     */
    private Integer port;

    /**
     * //用户名
     */
    private String userName;

    /**
     * //密码
     */
    private String pwd;

    /**
     * 默认超时时间
     */
    private Integer timedOut;

    /**
     * 模式 0 == ftp 和 1 == sftp
     */
    private String ftpType;
}

