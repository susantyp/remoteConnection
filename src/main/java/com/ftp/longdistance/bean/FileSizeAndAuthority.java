package com.ftp.longdistance.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tanyongpeng
 * <p>
 *     文件权限  文件大小
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileSizeAndAuthority {

    private String fileSize;

    private String authority;

}
