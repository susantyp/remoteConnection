package com.ftp.longdistance.ftpmapper.service;

import com.ftp.longdistance.bean.Ftp;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>
 *     功能点
 * </p>
 */
public interface PointOfJunction {

    /**
     * 退出登录
     */
    void logout();


    /**
     * 连接服务器
     * @param ftp
     * @return
     */
    boolean getConnect(Ftp ftp);


    /**
     * 文件上传
     * @param path
     * @param file
     */
    void upload(String path, MultipartFile file);

    /**
     * 文件下载
     * @param serverPath
     * @param fileName
     */
    void download(String serverPath,String fileName);


    /**
     * 创建文件夹
     * @param path
     */
    void dir(String path);


    /**
     * 删除文件夹
     * @param path
     */
    void deleteDir(String path);

    /**
     * 检查路径
     * @param filePath
     * @return
     */
    boolean checkExist(String filePath);

    /**
     * 检查文件 是非文件 || 文件夹
     * @param filePath
     * @return
     */
    boolean checkFileName(String filePath);


    /**
     * 删除文件
     * @param servicePath
     * @param fileName
     */
    void delete(String servicePath,String fileName);


    /**
     * 列出当前目录下的文件
     * @param directory
     * @return
     */
    List<String> listFiles(String directory);


    /**
     * 更改文件名称
     * @param oldPath
     * @param newPath
     */
    void rename(String oldPath,String newPath);


    /**
     * 获取文件权限
     * @param path
     * @return
     */
    String fileAuthority(String path);

    /**
     * 获取文件大小
     * @param path
     * @return
     */
    String fileSize(String path);


    /**
     * 预览文件
     * @param response
     * @param path
     * @param fileName
     */
    void fileView(HttpServletResponse response,String path,String fileName);

}
