package com.ftp.longdistance.ftpmapper;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.ftp.longdistance.annotation.FileType;
import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * @author TanYongPeng
 * <p>
 *     Ssh模式 案例
 * </p>
 */
@Slf4j
@FileType("SSH")
public enum SshFile {
    /**
     * this is SSH
     */
    INSTANCE;

    private static final String DEFAULT_CODE = "UTF-8";

    private static Connection conn = null;

    /**
     * 登录主机
     * @return
     *      登录成功返回true，否则返回false
     */
    public boolean login(String ip,Integer port, String userName,String userPwd){
        try {
            conn = new Connection(ip,port);
            //连接
            conn.connect();
            //认证
            conn.authenticateWithPassword(userName, userPwd);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 远程执行shll脚本或者命令
     * @param cmd
     *      即将执行的命令
     * @return
     *      命令执行完后返回的结果值
     */
    public String execute(String cmd){
        String result="";
        try {
            if(conn !=null){
                //打开一个会话
                Session session= conn.openSession();
                //执行命令
                session.execCommand(cmd);
                result=processStdout(session.getStdout(),DEFAULT_CODE);
                //如果为得到标准输出为空，说明脚本执行出错了
                if(StringUtils.isBlank(result)){
                    log.info("得到标准输出为空,链接conn:"+conn+",执行的命令："+cmd);
                    result=processStdout(session.getStderr(),DEFAULT_CODE);
                }else{
                    log.info("执行命令成功,链接conn:"+conn+",执行的命令："+cmd);
                }
                session.close();
            }
        } catch (IOException e) {
            log.info("执行命令失败,链接conn:"+conn+",执行的命令："+cmd+"  "+e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
    /**
     * 解析脚本执行返回的结果集
     * @param in 输入流对象
     * @param charset 编码
     * @return
     *       以纯文本的格式返回
     */
    private String processStdout(InputStream in, String charset){
        InputStream  stdout = new StreamGobbler(in);
        StringBuffer buffer = new StringBuffer();;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout,charset));
            String line=null;
            while((line=br.readLine()) != null){
                buffer.append(line+",");
            }
        } catch (UnsupportedEncodingException e) {
            log.error("解析脚本出错："+e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error("解析脚本出错："+e.getMessage());
            e.printStackTrace();
        }
        String result = buffer.toString();
        if (org.springframework.util.StringUtils.hasLength(buffer.toString())){
            result =  result.substring(0,result.length()-1);
        }
        return result;
    }

    public void logout(){
        try {
            if (conn != null){
                conn.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    


}
