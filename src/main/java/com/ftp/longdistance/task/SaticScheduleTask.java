package com.ftp.longdistance.task;

import com.ftp.longdistance.service.service.WebSocketService;
import com.ftp.longdistance.util.PathUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author tanyongpeng
 * <p>
 *     推送消息
 * </p>
 */
@Configuration
@EnableScheduling
@Slf4j
public class SaticScheduleTask {

    @Autowired
    WebSocketService webSocket;


    @Scheduled(cron = "0/5 * * * * ?")
    public void configureTasks(){
        webSocket.sendMessage(String.valueOf(webSocket.userCount()));
        if(webSocket.userCount() == 0){
            PathUtils.deleteFileViewPath();
        }
    }


}
