package com.ftp.longdistance.exception;

import com.ftp.longdistance.exception.service.BaseErrorInfoInterface;

/**
 * @author tanyongpeng
 */
public enum CommonEnum implements BaseErrorInfoInterface {
    // 数据操作错误定义
    SUCCESS("200", "操作成功"),
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),

    ;

    /** 错误码 */
    private String resultCode;

    /** 错误描述 */
    private String resultMsg;

    CommonEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }
}
