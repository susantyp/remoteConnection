package com.ftp.longdistance.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.json.JSONUtil;

import java.io.File;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>
 *     文件路径 工具类
 * </p>
 */
public class PathFileUtils<T> {

    public static boolean exists(String fileName){
        File file = new File(PathUtils.desktopDataJSONPath(fileName));
        return file.exists();
    }

    public List<T> lists(String fileName, Class cls){
        FileReader fileReader = new FileReader(PathUtils.desktopDataJSONPath(fileName));
        String fileContent = fileReader.readString();
        return JSONUtil.toList(fileContent, cls);
    }

    public void write(List<T> list,T t,String fileName){
        list.add(t);
        String toJsonStr = JSONUtil.toJsonStr(list);
        FileUtil.writeUtf8String(toJsonStr,PathUtils.desktopDataJSONPath(fileName));
    }

    public void write(List<T> list,String fileName){
        String toJsonStr = JSONUtil.toJsonStr(list);
        FileUtil.writeUtf8String(toJsonStr,PathUtils.desktopDataJSONPath(fileName));
    }

}
