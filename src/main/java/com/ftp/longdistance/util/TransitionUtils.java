package com.ftp.longdistance.util;

import com.ftp.longdistance.bean.vo.FileVo;
import com.ftp.longdistance.ftpmapper.service.PointOfJunction;
import com.ftp.longdistance.util.PathUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tanyongpeng
 * <p>
 *     转换fileVo对象 工具类
 * </p>
 */
@Slf4j
public enum TransitionUtils {
    /**
     * this
     */
    INSTANCE;


    public List<FileVo> transitionFileVo(String path, List<String> list, PointOfJunction p) {
        List<FileVo> voList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean flag = p.checkFileName(PathUtils.checkPath(path) + list.get(i));
            log.info("checkFileName = {}  isDir = {}", PathUtils.checkPath(path)+list.get(i),flag);
            voList.add(new FileVo(list.get(i),flag?1:0));
        }
        return voList;
    }
}
