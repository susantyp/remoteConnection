package com.ftp.longdistance.util;



import com.ftp.longdistance.bean.Ftp;
import com.ftp.longdistance.bean.vo.PathVo;
import com.ftp.longdistance.exception.BizException;


import com.ftp.longdistance.ftpmapper.service.PointOfJunction;

import com.ftp.longdistance.bean.vo.FileVo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author TanYongPeng
 * <p>
 *     路径 工具类
 * </p>
 */
@Slf4j
public class PathUtils {


    /**
     * 返回值
     * 第一个路径
     * 第二个名称
     * /home/web/
     * @param path
     * @return
     */
    public static String[] path(String path){
        String[] split = path.split("/");
        String[] array = new String[split.length];
        if (split.length == 2) {
            array[0] = "/";
            array[1] = split[1];
        }else {
            String newPath = path.substring(0,path.lastIndexOf("/")+1);
            String likePath = path.substring(path.lastIndexOf("/")+1);
            array[0] = newPath;
            array[1] = likePath;
        }
        return array;
    }

    /**
     * 判断最后一个字符是否带 / 不带则加上 /
     * @param path
     * @return
     */
    public static String checkPath(String path){
        path = path.replace("//","/");
        if (path.length()>=2) {
            String newPath = path.substring(path.length()-1);
            if (!"/".equals(newPath)){
                return path+"/";
            }
        }
        return path;
    }

    /**
     * 判断第一个字符是否带 / 不带则加上 /
     * @param path
     * @return
     */
    public static String firstCheckPath(String path){
        if (path.length()>=2) {
            String newPath = path.substring(0,1);
            if (!"/".equals(newPath)){
                return "/"+path;
            }
        }
        return path;
    }

    /**
     * 判断最后一个字符是否带 / 带 则去掉 /
     * @param path
     * @return
     */
    public static String lastCheckPath(String path){
        path = path.replace("//","/");
        if (path.length()>=2) {
            String newPath = path.substring(path.length()-1);
            if ("/".equals(newPath)){
                return path.substring(0, path.length() - 1);
            }
        }
        return path;
    }


    /**
     * 返回桌面路径
     * @param fileName
     * @return
     */
    public static String viewPath(String fileName){
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File com = fsv.getHomeDirectory();
        return com.getPath()+"\\"+fileName;
    }

    /**
     * 返回预览文件删除路径
     */
    public static void deleteFileViewPath(){
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File com = fsv.getHomeDirectory();
        File delFile = new File(com.getPath()+"\\预览临时文件");
        if (delFile.exists()) {
            remove(delFile);
            delFile.delete();
        }
    }

    /**
     * 迭代删除预览文件删除路径
     * @param file
     */
    public static void remove(File file) {
        File[] files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()) {
                    files[i].delete();
                } else if (files[i].isDirectory()) {
                    remove(files[i]);
                }
                files[i].delete();
            }
        }
    }

    /**
     * 获取预览文件
     * @return
     */
    private static String fileViewPath(){
        return desktopPath("预览临时文件");
    }

    /**
     * 返回预览文件地址
     * @param fileName
     * @return
     */
    public static String fileViewPath(String fileName){
        return fileViewPath()+"\\"+fileName;
    }


    /**
     * 返回存储数据文件地址
     * @return
     */
    public static String desktopDataJSONPath(String fileName){
        return desktopPath("数据文件")+"\\"+fileName;
    }

    /**
     * 返回桌面文件路径
     * @param name
     * @return
     */
    private static String desktopPath(String name){
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File com = fsv.getHomeDirectory();
        File file = new File(com.getPath()+"\\"+name);
        if (!file.exists()){
            file.mkdirs();
        }
        return com.getPath()+"\\"+name;
    }

    /**
     * 设置新的回退路径
     * @param path
     * @return
     */
    public static String newPath(String path){
        path = path.substring(0,path.lastIndexOf("/"));
        path = "".equals(path) ?"/":path;
        return path;
    }

    /**
     * 获取当前路径
     * @param request
     * @return
     */
    public static String currPath(HttpServletRequest request){
        return (String) request.getSession().getAttribute("currentDirectory");
    }

    /**
     * 存储当前路径
     * @param map
     * @param request
     * @param list
     */
    public static Map<String,Object> mapAddPath(Map<String, Object> map, HttpServletRequest request, List<FileVo> list) {
        map.put("currentDirectory", PathUtils.currPath(request));
        map.put("listPath",list);
        return  map;
    }

    /**
     * 存储当前路径和数据返回前端
     * @param voList
     * @param path
     * @param request
     */
    public static HashMap<String,Object> mapAddPath(List<FileVo> voList, PathVo path, HttpServletRequest request){
        HashMap<String,Object> map = new HashMap<>();
        request.getSession().setAttribute("currentDirectory", PathUtils.lastCheckPath(path.getPath()));
        map.put("currentDirectory",path.getPath());
        map.put("listPath",voList);
        log.info("listPath = {}",voList);
        log.info("path = {}",path);
        return map;
    }


    /**
     * 将路径拆分为 路径 和 文件名
     * 第一个路径
     * 第二个名称
     * @param path
     * @return
     */
    public static String[] split(String path){
        String[] array = new String[2];
        if (path.length()>=2){
            array[0] = path.substring(0,path.lastIndexOf("/")+1);
            array[1] = path.substring(path.lastIndexOf("/")+1);
        }
        return array;
    }



}
