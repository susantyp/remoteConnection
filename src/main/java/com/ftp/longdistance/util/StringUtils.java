package com.ftp.longdistance.util;

import com.ftp.longdistance.annotation.CommandName;
import com.ftp.longdistance.bean.vo.CommandVo;
import com.ftp.longdistance.exception.BizException;
import com.ftp.longdistance.service.service.FileFtpService;
import com.ftp.longdistance.service.service.SelectCommandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author TanYongPeng
 * <p>
 *     字符 工具类
 * </p>
 */
@Component
@Slf4j
public class StringUtils {

    /**
     * 获取注解信息
     * @return
     */
    private static Set<String> commandList(){
        Set<String> commandList = new HashSet<>();
        Class cls =  SelectCommandService.class;
        Method[] methods = cls.getMethods();
        for (Method method : methods) {
            CommandName annotation = method.getAnnotation(CommandName.class);
            commandList.add(annotation.comm());
        }
        return commandList;
    }


    /**
     * 处理命令字符
     * @param order
     * @return
     */
    public static String[] disposeChar(String order){
        try {
            String[] split = order.split(" ");
            List<String> collect = Arrays.stream(split).collect(Collectors.toList());
            collect.removeIf(l->l.equals("") || l.equals(" ") || null == l);
            return disposeChar(collect);
        }catch (Exception e){
            throw new BizException("110","命令字符处理异常");
        }
    }

    /**
     * 对比命令
     * 第一个命令值
     * 第二个路径
     * @param list
     * @return
     */
    public static String[] disposeChar(List<String> list){
        log.info("当前集合 {}",commandList());
        if (commandList().stream().anyMatch(a -> a.trim().equals(list.get(0).trim()))) {
            return new String[]{list.get(0).trim(),list.get(1).trim()};
        }
        throw new BizException("109","-bash: "+list.get(0).trim()+": command not found 命令未找到异常");
    }


}
