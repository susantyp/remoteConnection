package com.ftp.longdistance.exe;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.NumberFormat;

/**
 * @author tanyongpeng
 * <p>
 *     启动 跳转
 * </p>
 */
@Component
@Slf4j
public class SelfStarting {

    @Value("${systemSet.url:http://localhost:9099}")
    private String url;

    public void exec(boolean state){
        if (state && (!"Linux".equals(System.getProperty("os.name")))){
            try {
                Runtime.getRuntime().exec("cmd   /c   start "+url+" ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static PrintStream bef(){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        return System.out;
    }
}
