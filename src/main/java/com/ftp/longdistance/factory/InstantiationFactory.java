package com.ftp.longdistance.factory;

import com.ftp.longdistance.annotation.GetConnectionType;
import com.ftp.longdistance.ftpmapper.service.PointOfJunction;
import com.ftp.longdistance.service.service.FileFtpService;
import com.ftp.longdistance.service.service.LoginService;
import com.ftp.longdistance.service.service.SelectCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 *     返回 实例化 对象
 * </p>
 */
@Component
public class InstantiationFactory {


    @Autowired
    GetConnectionType<PointOfJunction> point;

    @Autowired
    GetConnectionType<LoginService> login;

    @Autowired
    GetConnectionType<FileFtpService> file;

    @Autowired
    GetConnectionType<SelectCommandService> comm;


    /**
     * 注解方式，返回不同的实例对象，移除了以往的if else 或switch
     * 使用：只需要在当前类上面标注 @FileType("FTP") 就会根据当前注解的值返回不同的实例对象
     * @param request
     * @return
     */
    public FileFtpService type(HttpServletRequest request){
        return file.getConnectionType((String) request.getSession().getAttribute("ftpType"), "com.ftp.longdistance.service.impl.connect");
    }

    public SelectCommandService command(HttpServletRequest request){
        return comm.getConnectionType((String) request.getSession().getAttribute("ftpType"), "com.ftp.longdistance.service.impl.connect");
    }

    private PointOfJunction typeFileMapperAll(String sessionFtpType,String ftpType){
        return point.getConnectionFileType(sessionFtpType == null?ftpType:sessionFtpType,"com.ftp.longdistance.ftpmapper");
    }


    private LoginService loginAll(String sessionFtpType,String ftpType){
        return login.getConnectionType(sessionFtpType == null?ftpType:sessionFtpType,"com.ftp.longdistance.service.impl.login");
    }




    public LoginService loginOut(HttpServletRequest request){
        return loginAll((String) request.getSession().getAttribute("ftpType"),null);
    }

    public LoginService login(String ftpType){
       return loginAll(null,ftpType);
    }

    public PointOfJunction fileLoginMapper(HttpServletRequest request){
        return typeFileMapperAll((String) request.getSession().getAttribute("ftpType"),null);
    }

    public PointOfJunction loginMapper(String ftpType){
        return typeFileMapperAll(null,ftpType);
    }


}
