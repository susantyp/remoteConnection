package com.ftp.longdistance.controller;


import com.ftp.longdistance.annotation.aspect.SystemEnvironment;
import com.ftp.longdistance.bean.*;
import com.ftp.longdistance.bean.vo.ExeVo;
import com.ftp.longdistance.exception.BizException;
import com.ftp.longdistance.result.Result;

import com.ftp.longdistance.service.impl.ExecuteCommand;
import com.ftp.longdistance.service.service.DeployExeService;
import com.ftp.longdistance.factory.InstantiationFactory;
import com.ftp.longdistance.bean.vo.PathVo;
import com.ftp.longdistance.util.PathUtils;
import lombok.extern.slf4j.Slf4j;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>
 *     当前控制器用于文件的处理
 * </p>
 */
@RestController
@Slf4j
public class FileDirController {

    @Autowired
    InstantiationFactory usageMode;

    @Autowired
    DeployExeService deployExe;

    @Autowired
    ExeVo exeVo;

    @Autowired
    ExecuteCommand executeCommand;

    @RequestMapping("/fileList")
    public Result getFileList(@RequestBody PathVo path, HttpServletRequest request){
        try {
            Map<String, Object> map = usageMode.type(request).allList(path,request);
            return Result.success(map,"");
        }catch (Exception e){
            throw new BizException();
        }
    }

    @SystemEnvironment
    @RequestMapping("/fileSizeAndAuthority")
    public Result fileSize(@RequestBody PathVo path, HttpServletRequest request){
        FileSizeAndAuthority fileSizeAndAuthority = new FileSizeAndAuthority(usageMode.type(request).fileSize(path.getPath(), request), usageMode.type(request).fileAuthority(path.getPath(), request));
        if (StringUtils.hasLength(fileSizeAndAuthority.getFileSize()) && StringUtils.hasLength(fileSizeAndAuthority.getAuthority())) {
            return Result.success(fileSizeAndAuthority,"获取成功");
        }else {
            throw new BizException("102","文件大小或文件权限获取异常");
        }
    }

    @SystemEnvironment
    @RequestMapping("/download")
    public Result download(@RequestParam String path,@RequestParam String fileName,HttpServletRequest request){
        try {
            usageMode.type(request).download(path,fileName);
            return Result.success(null,"已下载到桌面");
        }catch (Exception e){
            throw new BizException();
        }
    }

    @SystemEnvironment
    @RequestMapping("/upload")
    public Result upload(@RequestParam MultipartFile file,HttpServletRequest request){
        try{
            usageMode.type(request).upload(file,request);
            return Result.success(null,"上传成功");
        }catch (Exception e){
            throw new BizException("111","文件上传异常");
        }
    }

    @SystemEnvironment
    @RequestMapping("/fileDelete")
    public Result fileDelete(@RequestParam String filePath,@RequestParam String fileName,HttpServletRequest request){
        try{
            usageMode.type(request).fileDelete(filePath,fileName);
            return Result.success(null,"删除成功");
        }catch (Exception e){
            throw new BizException("104","文件删除异常");
        }
    }

    @SystemEnvironment
    @GetMapping("/sendTheCommand")
    public Result sendTheCommand(@RequestParam String order,HttpServletRequest request){
        try {
            Map<String, Object> map = executeCommand.executeCommand(order,request);
            return Result.success(map,"执行成功");
        }catch (Exception e){
            throw new BizException("105","-bash: "+order+": command execution failure");
        }
    }

    @SystemEnvironment
    @RequestMapping("/rename")
    public Result rename(@RequestParam String oldPath,@RequestParam String newPath,HttpServletRequest request){
        try{
            usageMode.type(request).rename(oldPath,newPath);
            return Result.success(null,"更改成功");
        }catch (Exception e){
            throw new BizException("107","文件名更改异常");
        }
    }

    @RequestMapping("/downExe")
    public void downExe(HttpServletRequest request, HttpServletResponse response){
        deployExe.downExe(exeVo.getPath(),exeVo.getFileName(),request,response);
    }

    @RequestMapping("/checkPath")
    public Result checkPath(@RequestParam String path,HttpServletRequest request){
        boolean b = usageMode.fileLoginMapper(request).checkExist(path);
        if (b){
            return Result.success();
        }else {
            throw new BizException("101","文件路径异常");
        }
    }

    @SystemEnvironment
    @RequestMapping("/fileView")
    public void fileView(HttpServletRequest request,HttpServletResponse response, @RequestParam String fileName){
        String path = (String) request.getSession().getAttribute("currentDirectory");
        usageMode.type(request).fileView(response,PathUtils.checkPath(path),fileName);
    }
}
