package com.ftp.longdistance.controller;
import com.ftp.longdistance.bean.vo.CommandListAllVo;
import com.ftp.longdistance.factory.InstantiationFactory;
import com.ftp.longdistance.service.service.ConnectionTypeService;
import com.ftp.longdistance.service.service.GetLocalhostAddressService;
import com.ftp.longdistance.util.PathUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author tanyongpeng
 * <p>
 *     当前控制器用于页面的跳转
 * </p>
 */
@Controller
@Slf4j
public class PageTurnController {

    @Autowired
    InstantiationFactory usageMode;

    @Autowired
    ConnectionTypeService connectionType;

    @Autowired
    CommandListAllVo commandListAllVo;

    @Autowired
    GetLocalhostAddressService getLocalhostAddressService;

    @Value("${systemSet.suffix}")
    private String suffix;

    @Value("${spring.profiles.active}")
    private String active;

    @GetMapping("/")
    public String toMain(Model model,HttpServletRequest request){
        model.addAttribute("requestUrl",getLocalhostAddressService.getLocalhost(request));
        return "main";
    }


    @RequestMapping("/index")
    public String toIndex(Model model,HttpServletRequest request){
        model.addAttribute("userName",request.getSession().getAttribute("userName"));
        model.addAttribute("time",request.getSession().getAttribute("time"));
        model.addAttribute("host",request.getSession().getAttribute("host"));
        model.addAttribute("suffix",suffix);
        model.addAttribute("connectType",request.getSession().getAttribute("ftpType"));
        model.addAttribute("requestUrl",getLocalhostAddressService.getLocalhost(request));
        model.addAttribute("webSocketHostPort",getLocalhostAddressService.getHostPort(request));
        return "principal";
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpServletRequest request){
        usageMode.loginOut(request).loginOut(request,usageMode.fileLoginMapper(request));
        return "redirect:/";
    }

    @RequestMapping("/welcome")
    public String welcome(Model model,HttpServletRequest request){
        model.addAttribute("active",active);
        model.addAttribute("requestUrl",getLocalhostAddressService.getLocalhost(request));
        return "welcome";
    }

    @RequestMapping("/adminList")
    public String adminList(Model model){
        model.addAttribute("active",active);
        return "admin-list";
    }



    @RequestMapping("/adminAdd")
    public String adminAdd(Model model){
        model.addAttribute("connectionTypeList",connectionType.connectionTypeList());
        return "admin-add";
    }

    @RequestMapping("/addBtn")
    public String addBtn(){
        return "addBtn";
    }

    @RequestMapping("/commandView")
    public String commandView(Model model){
        model.addAttribute("commandAllList",commandListAllVo.getList());
        return "commandView";
    }

    @RequestMapping("/addBtnManagement")
    public String addBtnManagement(){
        return "addBtnManagement";
    }
}
