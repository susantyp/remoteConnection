package com.ftp.longdistance.controller;

import com.ftp.longdistance.bean.Ftp;

import com.ftp.longdistance.exception.BizException;
import com.ftp.longdistance.result.Result;
import com.ftp.longdistance.factory.InstantiationFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>
 *     当前控制器用于登录
 * </p>
 */
@RestController
@Slf4j
public class FtpLoginController {

    @Autowired
    InstantiationFactory usageMode;

    /**
     * 登录
     * @param ftp
     * @return
     */
    @RequestMapping("/login")
    public Result login(@RequestBody Ftp ftp, HttpServletRequest request){
        long startTime = System.currentTimeMillis();
        boolean login = usageMode.login(ftp.getFtpType()).login(ftp, request,usageMode.loginMapper(ftp.getFtpType()));
        long endTime = System.currentTimeMillis();
        request.getSession().setAttribute("time",endTime-startTime);
        if (login) {
            return Result.success(null,"登录成功!");
        }else {
            throw new BizException();
        }
    }


}
