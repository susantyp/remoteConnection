package com.ftp.longdistance.controller;

import cn.hutool.json.JSONUtil;
import com.ftp.longdistance.annotation.aspect.SystemEnvironment;
import com.ftp.longdistance.bean.vo.BtnVo;
import com.ftp.longdistance.bean.vo.FtpVo;
import com.ftp.longdistance.result.Result;
import com.ftp.longdistance.service.service.SaveJsonLocalhostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

/**
 * @author TanYongPeng
 * <p>
 *     操作json文件
 * </p>
 */
@RestController
public class SaveJsonLocalhostController {

    @Autowired
    SaveJsonLocalhostService saveJsonLocalhostService;

    @SystemEnvironment
    @RequestMapping("/addLoginObj")
    public void addLoginObj(@RequestBody FtpVo ftpVo){
        saveJsonLocalhostService.addLoginObj(ftpVo);
    }

    @GetMapping("/loginList")
    public Result loginList(){
        return Result.success(saveJsonLocalhostService.loginList(),"");
    }

    @SystemEnvironment
    @RequestMapping("/delLoginObj/{id}")
    public void delLoginObj(@PathVariable String id){
        saveJsonLocalhostService.delLoginObj(id);
    }

    @SystemEnvironment
    @RequestMapping("/addBtnObj")
    public void addBtnObj(@RequestBody BtnVo btnVo){
        saveJsonLocalhostService.addBtnObj(btnVo);
    }

    @RequestMapping("/btnList/{host}")
    public Result btnList(@PathVariable String host){
       return Result.success(saveJsonLocalhostService.btnList(host),"");
    }

    @SystemEnvironment
    @RequestMapping("/delBtnObj/{host}/{btnId}")
    public void delBtnObj(@PathVariable String host,@PathVariable String btnId){
        saveJsonLocalhostService.delBtnObj(host,btnId);
    }
}
