package com.ftp.longdistance.config;

import com.ftp.longdistance.LongDistanceApplication;
import com.ftp.longdistance.exe.SelfStarting;

import com.ftp.longdistance.system.SystemSettings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
/**
 * @author tanyongpeng
 * <p>
 *     启动类
 * </p>
 */
@Slf4j
@Component
public class  CommandLineRunnerImpl implements CommandLineRunner {


    @Value("${systemSet.self-starting:false}")
    private boolean selfStarting;


    @Autowired
    SelfStarting sel;

    private PrintStream printStream;

    public void run(Class<?> primarySource,String... args){
        if (!SystemSettings.sys.isOpenTheLog()){performBefore();}
        SpringApplication.run(primarySource, args);
    }
    /**
     * 执行前
     * @return
     */
    public void performBefore(){
        printStream = SelfStarting.bef();
    }
    /**
     * 执行后
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        if (!SystemSettings.sys.isOpenTheLog()){System.setOut(printStream);}
        sel.exec(selfStarting);
    }
}
