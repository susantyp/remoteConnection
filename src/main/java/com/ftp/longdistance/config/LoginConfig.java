package com.ftp.longdistance.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author tanyongpeng
 * <p>
 *     不拦截的资源
 * </p>
 */
@Configuration
public class LoginConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/","/toLogin","/delLoginObj/*","/loginList","/downExe","/addLoginObj","/adminList","/adminAdd","/welcome","/login","/**/*.html","/**/*.js/"
                ,"/**/*.eot","/**/*.svg","/**/*.ttf","/**/*.woff"
                ,"/**/*.css","/**/*.ico","/**/*.jpg","/**/*.png");
    }
}
