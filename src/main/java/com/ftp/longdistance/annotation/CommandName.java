package com.ftp.longdistance.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author tanyongpeng
 * <p>
 *     comm 必填项  核对当前是否有这个命令，从而抛出异常，限制往下执行
 * </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandName {

    String comm();

    String des() default "";

}
