package com.ftp.longdistance.annotation.aspect;

import java.lang.annotation.*;

/**
 * @author tanyongpeng
 * <p>
 * 系统环境判定
 * </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemEnvironment {
}
