package com.ftp.longdistance.annotation.aspect;

import com.ftp.longdistance.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

/**
 * @author tanyongpeng
 * <p>
 * 切面类
 * </p>
 */
@Aspect
@Component
@Slf4j
public class RequestAspect {

    @Before(value = "@annotation(com.ftp.longdistance.annotation.aspect.SystemEnvironment)")
    public void doBefore(JoinPoint joinPoint) {
        //获取到请求的属性
        ServletRequestAttributes attributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取到请求对象
        HttpServletRequest request = attributes.getRequest();
        log.info("系统环境 = {}",System.getProperty("os.name"));
        if ("Linux".equals(System.getProperty("os.name"))){
            throw new BizException("113","线上无权限操作！请拉取代码或部署exe");
        }
    }


}
