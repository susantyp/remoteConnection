package com.ftp.longdistance.annotation;

import java.lang.annotation.*;

/**
 * @author tanyongpeng
 * <p>
 * 标识一个为某某（FTP,SFTP）连接类型，通过GetConnectionType 反射创建对象
 * </p>
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FileType {

    String value();
}
