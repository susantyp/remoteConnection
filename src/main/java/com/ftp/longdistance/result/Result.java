package com.ftp.longdistance.result;

import com.ftp.longdistance.exception.CommonEnum;
import com.ftp.longdistance.exception.service.BaseErrorInfoInterface;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author tanyongpeng
 * <p>
 *     返回结果
 * </p>
 */
@Data
public class Result {

    private String code;
    private String msg;
    private Object data;
    private String date;

    public static Result success(Object data,String msg) {
        return success(CommonEnum.SUCCESS.getResultCode(),msg,data,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result success(Object data) {
        return success(CommonEnum.SUCCESS.getResultCode(),CommonEnum.SUCCESS.getResultMsg(),data,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result success() {
        return success(CommonEnum.SUCCESS.getResultCode(),CommonEnum.SUCCESS.getResultMsg(),null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result success(String code, String msg, Object data,String date) {
       Result r = new Result();
       r.setCode(code);
       r.setMsg(msg);
       r.setData(data);
       r.setDate(date);
       return r;
    }

    public static Result fail(BaseErrorInfoInterface e) {
        return fail(e.getResultCode(),e.getResultMsg(),null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result fail(String code,String msg) {
        return fail(code,msg,null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result fail(String msg) {
        return fail("500",msg,null,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }

    public static Result fail(String code, String msg, Object data,String date) {
        Result r = new Result();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        r.setDate(date);
        return r;
    }


}
