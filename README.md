# Remote Connection(以交流学习为目的)

#### 介绍
该平台类似于网页版的Xshell，基于cookie的方式存储登录信息无数据库更方便兼容每个用户的使用，支持SFTP和FTP两种登录方式，打破原有黑端显示命令行和文件名的方式，以div的方式展示文件图，支持命令输入，报错输出

#### 软件架构
SpringBoot jcraft Layui Cookie WebSocket 等等


#### 安装教程

拉取代码后，直接点击LongDistanceApplication main方法运行即可

#### 使用说明

1. 拉取代码加载当前依赖包
2. 运行主函数main方法即可
3. 或者打包jar包，在Windows命令窗口下 java -jar jar包路径/remote-connection.jar

### 图文教程
<a href="https://blog.csdn.net/Susan003/article/details/122754995?spm=1001.2014.3001.5501">点击跳转，查看详细教程</a>

#### 登录图
![登录图](src/main/resources/static/images/image.png)
#### 首页图
![首页图](src/main/resources/static/images/image2.png) 